from bottle import route, run, template

@route('/<name>/<age>')
def person(name, age):
    return template('<b>{{name}} is {{age}} years old</b>', name=name, age=age)

run(host='localhost', port=8080, debug=True)